OF_ROOT = .

LIBSPATH=linux
INCLUDES_FLAGS += -I$(OF_ROOT)/libs/glu/include
INCLUDES_FLAGS += $(shell pkg-config gl glew glu gstreamer-0.10 gstreamer-video-0.10 gstreamer-base-0.10 libudev cairo --cflags-only-I )

#check if gtk exists and add it
GTK = $(shell pkg-config gtk+-2.0 --exists; echo $$?)
ifeq ($(GTK),0)
	CFLAGS += $(shell pkg-config gtk+-2.0 --cflags-only-I ) -DOF_USING_GTK
	SYSTEMLIBS += $(shell pkg-config gtk+-2.0 --libs-only-l)
endif

#check if mpg123 exists and add it
MPG123 = $(shell pkg-config libmpg123 --exists; echo $$?)
ifeq ($(MPG123),0)
	CFLAGS += -DOF_USING_MPG123
	SYSTEMLIBS += -lmpg123
endif

SYSTEMLIBS += $(shell pkg-config jack glu glew gstreamer-0.10 gstreamer-video-0.10 gstreamer-base-0.10 gstreamer-app-0.10 libudev cairo zlib --libs-only-l)
SYSTEMLIBS += -lglut -lGL -lasound -lopenal -lsndfile -lvorbis -lFLAC -logg -lfreeimage 
LIB_STATIC += $(OF_ROOT)/libs/poco/lib/$(LIBSPATH)/libPocoNet.a $(OF_ROOT)/libs/poco/lib/$(LIBSPATH)/libPocoXML.a $(OF_ROOT)/libs/poco/lib/$(LIBSPATH)/libPocoUtil.a $(OF_ROOT)/libs/poco/lib/$(LIBSPATH)/libPocoFoundation.a

CORE_INCLUDES = $(shell find $(OF_ROOT)/libs/openFrameworks/ -type d)
CORE_INCLUDE_FLAGS = $(addprefix -I,$(CORE_INCLUDES))
INCLUDES = $(shell find $(OF_ROOT)/libs/*/include -type d | grep -v glu | grep -v quicktime | grep -v poco)
INCLUDES_FLAGS += $(addprefix -I,$(INCLUDES))
INCLUDES_FLAGS += -I$(OF_ROOT)/libs/poco/include

LIB_STATIC += $(shell ls $(OF_ROOT)/libs/*/lib/$(LIBSPATH)/*.a  2> /dev/null | grep -v openFrameworksCompiled | grep -v Poco)
LIB_SHARED += $(shell ls $(OF_ROOT)/libs/*/lib/$(LIBSPATH)/*.so  2> /dev/null | grep -v openFrameworksCompiled | sed "s/.*\\/lib\([^/]*\)\.so/-l\1/")
LIB_PATHS_FLAGS = $(shell ls -d $(OF_ROOT)/libs/*/lib/$(LIBSPATH) | sed "s/\(\.*\)/-L\1/")

CFLAGS += -I.
CFLAGS += $(INCLUDES_FLAGS)
CFLAGS += $(CORE_INCLUDE_FLAGS)

TARGET_LIBS = $(OF_ROOT)/libs/openFrameworksCompiled/lib/$(LIBSPATH)/libopenFrameworks.a

testapp: ./src/*.cpp ./src/*.cu
	nvcc ./src/*.cu -c -lcuda -lcudart -I /home/deepak/NVIDIA_GPU_Computing_SDK/C/common/inc  -I ./libs/opencv/include/opencv
	nvcc ./src/*.cpp *.o  -o testApp $(CFLAGS) $(TARGET_LIBS) $(LIB_STATIC) $(LIB_PATHS_FLAGS) $(LIB_SHARED) $(SYSTEMLIBS) 
