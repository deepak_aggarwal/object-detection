#pragma once

#include "ofMain.h"
#include "optflowlk.h"
#include "fastBgGMM.h"

class testApp : public ofBaseApp{
	
	public:
		
		void setup();
		void update();
		void draw();
		
		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);		
		
		ofVideoGrabber 		vidGrabber;
		ofTexture		videoTexture;
		int 			camWidth;
		int 			camHeight;
		int totalPixels;
		unsigned char * 	previousFrame;			// Holds previous frame 
		unsigned char * 	currentFrame;			// Holds current frame
		unsigned char * 	updateFrame;			// used for exchanging frame
		unsigned char *		oldDifferenceFrame;
		unsigned char * 	differenceFrame;		// Used for holding difference frame pixels.
		unsigned char *		frameAccum;			// Used for background learning.
		ofTexture		differenceTexture;		// Used for displaying difference frame.d
		ofTexture		opticalFlow_X_texture;
		ofTexture		opticalFlow_Y_texture;
		float			opticalFlow_X_velocity[15][76800];
		float			opticalFlow_Y_velocity[15][76800];

		int 			opticalFlow_X_cumulative_positive[76800];
		int 			opticalFlow_Y_cumulative_positive[76800];
		int 			opticalFlow_X_cumulative_negative[76800];
		int 			opticalFlow_Y_cumulative_negative[76800];

		unsigned char * 	opticalFlow_X;
		unsigned char * 	opticalFlow_Y;
		CvSize			matrixSize;
		CvSize			winSize;
		int 			currentOpticalFlowFrame;
		float 			motion_threshold;
		CvFastBgGMMParams 	*params;
		CvFastBgGMM 		*pGMM;
		IplImage		*videoPixels;

};
