/*
typedef struct CvSize
{
    int width;
    int height;
}
CvSize;

*/
#include <cv.h>
void cvCalcOpticalFlowLK( const void* srcarrA, const void* srcarrB,int step,CvSize matrixSize, CvSize winSize,
                     void* velarrx, void* velarry );
