#include "testApp.h"
#include<math.h>
#include <iostream>


//--------------------------------------------------------------
void testApp::setup(){
	
	camWidth 		= 320;	// try to grab at this size. 
	camHeight 		= 240;

	totalPixels = camWidth*camHeight*3;

	matrixSize.width = camWidth;
	matrixSize.height = camHeight;
	winSize.width = 13;
	winSize.height = 13;	
	
	vidGrabber.setVerbose(true);
	vidGrabber.initGrabber(camWidth,camHeight);
	
	previousFrame 	= new unsigned char[camWidth*camHeight];
	currentFrame 	= new unsigned char[camWidth*camHeight];
	differenceFrame = new unsigned char[camWidth*camHeight];
	oldDifferenceFrame = new unsigned char[camWidth*camHeight];
	frameAccum 	= new unsigned char[camWidth*camHeight];
	opticalFlow_X 	= new unsigned char[camWidth*camHeight];
	opticalFlow_Y 	= new unsigned char[camWidth*camHeight];
	videoTexture.allocate(camWidth,camHeight, GL_LUMINANCE);
	differenceTexture.allocate(camWidth,camHeight, GL_LUMINANCE);
	opticalFlow_X_texture.allocate(camWidth,camHeight, GL_LUMINANCE);
	opticalFlow_Y_texture.allocate(camWidth,camHeight, GL_LUMINANCE);
	for(int i=0;i< camWidth*camHeight ;i++)	
		frameAccum[i]=0;	
	
	currentOpticalFlowFrame = -1;	
	motion_threshold = 9;

	videoPixels = cvCreateImage(cvSize(camWidth,camHeight), IPL_DEPTH_8U, 3);

	params = cvCreateFastBgGMMParams(camWidth, camHeight);
	params->fAlphaT = 0.010f;
	vidGrabber.grabFrame();
	videoPixels->imageData = (char *) vidGrabber.getPixels();
	pGMM = cvCreateFastBgGMM(params, videoPixels);

}

//--------------------------------------------------------------
void testApp::update(){

	ofBackground(100,100,100);

	vidGrabber.grabFrame();
	
	if (vidGrabber.isFrameNew()){
		// Exchanging Frame
		updateFrame   = previousFrame;
		previousFrame = currentFrame;
		currentFrame  = updateFrame;

		updateFrame = oldDifferenceFrame;
		oldDifferenceFrame = differenceFrame;
		differenceFrame = updateFrame;
			
		videoPixels->imageData = (char *) vidGrabber.getPixels();
		
		cvUpdateFastBgGMM(pGMM, videoPixels);

		for (int i = 0; i < totalPixels; i=i+3){
			currentFrame[i/3] = 0.3*((unsigned char)videoPixels->imageData[i])+ 0.59*((unsigned char) videoPixels->imageData[i+1]) + 0.11*((unsigned char) videoPixels->imageData[i+2]);
		}

		/*
		for (int i = 0; i < totalPixels/3; i=i++){
			frameAccum[i] = 0.5*frameAccum[i] + 0.5 * abs(currentFrame[i]-previousFrame[i]);
			if(frameAccum[i] > 15)
				differenceFrame[i] = 255;
			else
				differenceFrame[i] = 0;
		}
		*/

		for (int i = 0; i < totalPixels/3; i=i++)
			differenceFrame[i] = (unsigned char )pGMM->h_outputImg->imageData[i];

		// Updating optical frame.
		currentOpticalFlowFrame++;
		if(currentOpticalFlowFrame > 15)
			currentOpticalFlowFrame = 0;

		cvCalcOpticalFlowLK( oldDifferenceFrame, differenceFrame, camWidth,matrixSize,winSize,
                     opticalFlow_X_velocity[currentOpticalFlowFrame], opticalFlow_Y_velocity[currentOpticalFlowFrame] );

		for (int i = 0; i < totalPixels/3; i=i++){
			opticalFlow_X_cumulative_positive[i] = 0;
			opticalFlow_Y_cumulative_positive[i] = 0;
			opticalFlow_X_cumulative_negative[i] = 0;
			opticalFlow_Y_cumulative_negative[i] = 0;
			opticalFlow_X[i] = 0;
			opticalFlow_Y[i] = 0;
		}	
	
		for (int i = 0; i < totalPixels/3; i=i++){
			for (int j = 0; j < 15; j++){
				if(opticalFlow_X_velocity[j][i] > 0)
					opticalFlow_X_cumulative_positive[i]++;
				else if(opticalFlow_X_velocity[j][i] < 0)
					opticalFlow_X_cumulative_negative[i]++;

				if(opticalFlow_Y_velocity[j][i] > 0)
					opticalFlow_Y_cumulative_positive[i]++;
				else if(opticalFlow_Y_velocity[j][i] < 0)
					opticalFlow_Y_cumulative_negative[i]++;
			}
		}

		for (int i = 0; i < totalPixels/3; i=i++){
			if(opticalFlow_X_cumulative_positive[i] > motion_threshold)
				opticalFlow_X[i]=255;
			if(opticalFlow_Y_cumulative_positive[i] > motion_threshold)
				opticalFlow_Y[i]=255;
			if(opticalFlow_X_cumulative_negative[i] > motion_threshold)
				opticalFlow_X[i]+=255;
			if(opticalFlow_Y_cumulative_negative[i] > motion_threshold)
				opticalFlow_Y[i]+=255;
		}
		
		opticalFlow_X_texture.loadData(opticalFlow_X, camWidth,camHeight, GL_LUMINANCE);
		opticalFlow_Y_texture.loadData(opticalFlow_Y, camWidth,camHeight, GL_LUMINANCE);	
		videoTexture.loadData(currentFrame, camWidth,camHeight, GL_LUMINANCE);
		differenceTexture.loadData((unsigned char *)pGMM->h_outputImg->imageData, camWidth,camHeight, GL_LUMINANCE);		
	}

}

//--------------------------------------------------------------
void testApp::draw(){
	ofSetHexColor(0xffffff);
	vidGrabber.draw(20,20);
	videoTexture.draw(20+camWidth,20,camWidth,camHeight);
	differenceTexture.draw(20,camHeight+20,camWidth,camHeight);

	opticalFlow_X_texture.draw(20,(camHeight+20)*2,camWidth,camHeight);
	opticalFlow_Y_texture.draw(20+camWidth,(camHeight+20)*2,camWidth,camHeight);
}


//--------------------------------------------------------------
void testApp::keyPressed  (int key){ 
	
	// in fullscreen mode, on a pc at least, the 
	// first time video settings the come up
	// they come up *under* the fullscreen window
	// use alt-tab to navigate to the settings
	// window. we are working on a fix for this...
	
	if (key == 's' || key == 'S'){
		vidGrabber.videoSettings();
	}
	
	
}

//--------------------------------------------------------------
void testApp::keyReleased(int key){ 
	
}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){
	
}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){
	
}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){
}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo){ 

}
